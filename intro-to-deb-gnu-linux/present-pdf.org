#+TITLE: Introduction to Debian GNU/Linux
#+DATE: 12 February 2023
#+AUTHOR: Abraham Raji
#+LICENSE: Creative Commons - CC-BY-SA v4.0
#+OPTIONS:   H:2 num:nil ^:{} toc:nil
#+DESCRIPTION: Btw I use Debian

* Intro
** Hey Everyone 👋!
- My name is [[https://abrahamraji.in][Abraham Raji]]
- I'm known online as [[mailto:avronr@tuta.io][avronr]].
- Pays bills by writting software.
- Web. Design. Cloud.
- Feel free to interupt me.
- No doubts are stupid. People who think otherwise are stupid.
* Basics of Debian
** Basics of Debian
- Debian GNU/Linux is a free as in free speech operating system.
- The Debian Project is an association of individuals who have made common cause to create a free operating system.
** Basics of Debian: The Debian Social Contract
- https://www.debian.org/social_contract

** Basics of Debian: The Debian Social Contract
- 1. Debian will remain 100% free

We provide the guidelines that we use to determine if a work is "free" in the document entitled "The Debian Free Software Guidelines". We promise that the Debian system and all its components will be free according to these guidelines. We will support people who create or use both free and non-free works on Debian. We will never make the system require the use of a non-free component.

** Basics of Debian: The Debian Social Contract
- 2. We will give back to the free software community

When we write new components of the Debian system, we will license them in a manner consistent with the Debian Free Software Guidelines. We will make the best system we can, so that free works will be widely distributed and used. We will communicate things such as bug fixes, improvements and user requests to the "upstream" authors of works included in our system.

** Basics of Debian: The Debian Social Contract
- 3. We will not hide problems

We will keep our entire bug report database open for public view at all times. Reports that people file online will promptly become visible to others.

** Basics of Debian: The Debian Social Contract
- 4. Our priorities are our users and free software

We will be guided by the needs of our users and the free software community. We will place their interests first in our priorities. We will support the needs of our users for operation in many different kinds of computing environments. We will not object to non-free works that are intended to be used on Debian systems, or attempt to charge a fee to people who create or use such works. We will allow others to create distributions containing both the Debian system and other works, without any fee from us. In furtherance of these goals, we will provide an integrated system of high-quality materials with no legal restrictions that would prevent such uses of the system.

** Basics of Debian: The Debian Social Contract
- 5. Works that do not meet our free software standards

We acknowledge that some of our users require the use of works that do not conform to the Debian Free Software Guidelines. We have created "contrib" and "non-free" areas in our archive for these works. The packages in these areas are not part of the Debian system, although they have been configured for use with Debian. We encourage CD manufacturers to read the licenses of the packages in these areas and determine if they can distribute the packages on their CDs. Thus, although non-free works are not a part of Debian, we support their use and provide infrastructure for non-free packages (such as our bug tracking system and mailing lists). The Debian official media may include firmware that is otherwise not part of the Debian system to enable use of Debian with hardware that requires such firmware.

* Basics of Debian: The Debian Free Software Guidelines
** Basics of Debian: The Debian Free Software Guidelines

- 1. Free Redistribution

The license of a Debian component may not restrict any party from selling or giving away the software as a component of an aggregate software distribution
containing programs from several different sources. The license may not require a royalty or other fee for such sale.

** Basics of Debian: The Debian Free Software Guidelines
- 2. Source Code

The program must include source code, and must allow distribution in source code as well as compiled form.

** Basics of Debian: The Debian Free Software Guidelines
- 3. Derived Works

The license must allow modifications and derived works, and must allow them to be distributed under the same terms as the license of the original software.

** Basics of Debian: The Debian Free Software Guidelines
- 4. Integrity of The Author's Source Code

The license may restrict source-code from being distributed in modified form only if the license allows the distribution of "patch files" with the source code for the purpose of modifying the program at build time. The license must explicitly permit distribution of software built from modified source code. The license may require derived works to carry a different name or version number from the original software. (This is a compromise. The Debian group encourages all authors not to restrict any files, source or binary, from being modified.)

** Basics of Debian: The Debian Free Software Guidelines
- 5. No Discrimination Against Persons or Groups

The license must not discriminate against any person or group of persons.

** Basics of Debian: The Debian Free Software Guidelines
- 6. No Discrimination Against Fields of Endeavor

The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research.
** Basics of Debian: The Debian Free Software Guidelines
- 7. Distribution of License

The rights attached to the program must apply to all to whom the program is redistributed without the need for execution of an additional license by those parties.

** Basics of Debian: The Debian Free Software Guidelines
- 8. License Must Not Be Specific to Debian

The rights attached to the program must not depend on the program's being part of a Debian system. If the program is extracted from Debian and used or distributed without Debian but otherwise within the terms of the program's license, all parties to whom the program is redistributed should have the same rights as those that are granted in conjunction with the Debian system.
** Basics of Debian: The Debian Free Software Guidelines
- 9. License Must Not Contaminate Other Software

The license must not place restrictions on other software that is distributed along with the licensed software. For example, the license must not insist that all other programs distributed on the same medium must be free software.
** Basics of Debian: The Debian Free Software Guidelines
- 10. Example Licenses

The "GPL", "BSD", and "Artistic" licenses are examples of licenses that we consider "free".
* Basics of Debian: Debian Constitution
** Basics of Debian: Debian Constitution

- Constitution for the Debian Project (v1.9)
- Describes organizational structure for formal decision-making in the Project.
- Does not describe the goals of the Project or how it achieves them, or contain any policies except those directly related to the decision-making process.
- Actors in the Project:
  - The Developers
  - The Project Leader
  - The Technical Committee
  - The individual Developer working on a particular task
  - Delegates appointed by the Project Leader for specific tasks
  - The Project Secretary.

* Basics of Debian:
** Basics of Debian:
- This operating system that we have created is called Debian GNU/Linx.
- Debian systems currently use the Linux kernel. 
- Three main distributions
  1. Stable (Bullseye, released August 14 2021, end of life 2026)
  2. Testing (Bookworm, ETA 2023)
  3. Unstable (Sid, Forever)
  4. Old Stable (Buster, released July 6 2019, end of life 2024)
  5. Oldoldstable (Stretch, released June 17 2017, EOL 2020)
  6. Experimental
* Contributing to Debian
** Contributing to Debian
- User
- Evangelist
- Contributor
- Debian maintainer (Package maintainer)

- Debian Developer (Official member of the Project)
* Technical Overview of Bullseye: Some Stats
** Technical Overview of Bullseye: Some Stats
- Available in 8 Architecture.
- Support for 78 languages.
- Default Desktop Environment Gnome has more than 37 languages with at least 80% of it is translated.
- 6 officially supported desktop environments.
- Grub2 is the default boot-loader.
* Technical Overview of Bookworm: non-free firmware is now part of Debian
** Technical Overview of Bookworm: non-free firmware is now part of Debian
- General resolution: https://www.debian.org/vote/2022/vote_003
  - Winner: Change SC for non-free firmware in installer, one installer
* Technical Overview of Bookworm: Core Packages
** Technical Overview of Bookworm: Core Packages
|--------------+------------+---------------------------|
| Software     |    Version | Note                      |
|--------------+------------+---------------------------|
| Linux Kernel | 6.1 series | Now rusty                 |
| Xorg         |     7.7+23 | the current default       |
| systemd      |      252.5 | If E-Corp made a software |
| Busybox      |     1.35.0 | The MVP                   |
| apt          |      2.5.6 | Mx.Worldwide              |
|--------------+------------+---------------------------|
* Technical Overview of Bookworm: Desktop Environments and Window Managers
** Technical Overview of Bookworm: Desktop Environments and Window Managers
|-----------------+---------+--------------------------------------|
| Software        | Version | Note                                 |
|-----------------+---------+--------------------------------------|
| Gnome (default) |    43.1 | macOS but less evil                  |
| KDE Plasma      |    5.26 | KaaS (trademark pending) [1]         |
| XFCE            |    4.18 | The actual Kitchen Sink              |
| LXQt            |     1.2 | Thing 1                              |
| LXDE            |      11 | Thing 2                              |
| Mate            |    4.16 | Retro Life                           |
|-----------------+---------+--------------------------------------|
| i3              |    4.22 | People's Window Manager              |
| sway            |     1.7 | compositor with WM powers on Wayland |
|-----------------+---------+--------------------------------------|
[1] - Kitchen sink As A Streamlined service
* Technical Overview of Bookworm: General Purpose Applications
** Technical Overview of Bookworm: General Purpose Applications
|-------------+---------+--------------------------------|
| Software    | Version | Note                           |
|-------------+---------+--------------------------------|
| GNU Emacs   |    28.2 | The only software that matters |
| GNU Nano    |     7.2 | If you hate parenthesis        |
| Vim         |     9.0 | sigh                           |
| Neovim      |     0.7 | One wasn't enough              |
| GCC         |    12.2 | Still very much alive!         |
| Python      |    3.11 | For when you can't "C"         |
| Bash        |  5.2.15 | More GNU in your life          |
| Git         |    2.39 | We're not savages.             |
| Firefox     |   102.7 | Where is all my RAM            |
| LibreOffice |     7.4 | To watch your stonks grow      |
|-------------+---------+--------------------------------|
* Getting Debian
** Getting Debian
- Get a Debian stable ISO
  - https://www.debian.org/CD/http-ftp/#stable
  - https://cdimage.debian.org/debian-cd/
- Get a Debian testing ISO
  - https://cdimage.debian.org/cdimage/weekly-builds/
* Getting Help
** Getting Help
- https://www.debian.org/releases/stable/
- IRC: https://wiki.debian.org/IRC
- Mailing List: https://lists.debian.org/debian-user/
- FSCI Informal Support Group: https://matrix.to/#/#fsci-support:poddery.com
  fsci.in
* The End
** The End
 Thanks for listening!

 - IRC: abrahamr
 - Email: work@abrahamr.in, abraham@debian.org
